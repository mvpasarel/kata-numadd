<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class NumAddSpec extends ObjectBehavior
{
//    function it_is_initializable()
//    {
//        $this->shouldHaveType('NumAdd');
//    }
//
//    function it_invalidates_number_189()
//    {
//        $this->shouldThrow('\InvalidArgumentException')->during('run', array(189));
//    }
//
//    function it_shows_none_solutions_for_2555()
//    {
//        $this->run(2555)->shouldReturn(216);
//    }

    function it_shows_solutions_for_1893()
    {
        $this->run(1893)->shouldReturn(1512);
    }
}
