<?php

class NumAdd
{

    public function run($number)
    {
        // lowest sum of 3 digit numbers (100+100+100) = 300
        // lowest sum of 3 different digit numbers (147+258+369) = 774
        // highest sum of 3 digit numbers (999+999+999) = 2997
        // highest sum of 3 different digit numbers (963+852+741) = 2556
        if($number < 774 || $number > 2556) {

            throw new InvalidArgumentException('Invalid number');
        }

        $sums = [];

        for($i = 102; $i <= 987; $i++) {
            if(strlen(count_chars($i, 3)) < 3) {
                continue;
            }
            for($j = 987; $j >= 102; $j--) {
                if(strlen(count_chars($j, 3)) < 3) {
                    continue;
                }
                if(similar_text($i, $j)) {
                    continue;
                }
                $k = (int)$number - (int)$i - (int)$j;

                if(strlen(count_chars($k, 3)) < 3) {
                    continue;
                }
                if($k < 102 || $k > 987) {
                    continue;
                }

                if(similar_text($i, $k) || similar_text($j, $k)) {
                    continue;
                }
                $sum = [$i, $j, $k];

                if(!in_array($sum,$sums)) {
                    $sums[] = $i . ' + ' . $j  . ' + ' . $k . " = " . ($i+$j+$k);
                }
            }
        }
        file_put_contents('result.txt', implode("\n",$sums));
        return count($sums);
    }

}
